# NewtonRaphsonTest

#### 介绍

用于探究多类NewtonRaphson迭代方法的收敛效率。

Reference文件夹包含参考资料，Example文件夹包含七层框架结构算例

本项目已与其他项目归并，不再维护，请移步[OpenSEES实例集锦](https://gitee.com/civilwilson/OpenSEES_Examples)

#### 软件架构

极其简单：AlgorithmSet, Algorithm, AlgorithmBasic

#### 依赖项

1. PGMHelper.dll

#### OpenSEES框架例子

1. 位于Example文件夹；

2. OSG、OSIDA、OSP均为主入口；

3. OSP用于模态分析，确定结构是否存在机构且刚度是否与预期相似；

4. OSG用于重力荷载计算，确定结构荷载是否转换正确且是否存在明显机构；

5. OSIDA用于动力增量弹塑性时程分析；

6. Function储存实用函数。

#### 相关推送

1. [【OpenSEES】浅析Newton迭代（一）：减少刚度重构工作量与减少迭代次数谁更有意义？](https://mp.weixin.qq.com/s/H31vgTdGQt3gN8HXCXX0aQ)

2. [【OpenSEES】浅析Newton迭代（二）：基于LineSearch优化迭代算法](https://mp.weixin.qq.com/s/bxFRFk7I4rrmu68mz-X1bw)