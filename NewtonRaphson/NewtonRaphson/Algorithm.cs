﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using PGMHelper;
using System.Windows.Forms.DataVisualization.Charting;

namespace NewtonRaphson
{
    /// <summary>
    /// 每种迭代方法的信息记录
    /// </summary>
    public class Algorithm
    {
        /// <summary>
        /// 讯息列表
        /// </summary>
        private List<AlgorithmBasic> infoList { set; get; }

        /// <summary>
        /// 序列名称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 获得数据
        /// </summary>
        /// <param name="isTime"></param>
        /// <returns></returns>
        public GridViewDatas GetData()
        {
            //初始化
            var data = new GridViewDatas();
            //添加数据
            data.Add(new GridViewColumn(this.infoList.GetPropertyList("Time").ConvertAll(t => (float)t),
                titleName: this.Name + "_Time", demical: 1));
            //添加数据
            data.Add(new GridViewColumn(this.infoList.GetPropertyList("TimeCost").ConvertAll(t => (int)t),
                titleName: string.Format("{0}_TimeCost", this.Name)));
            //添加数据
            data.Add(new GridViewColumn(this.infoList.GetPropertyList("IterationNumber").ConvertAll(t => (int)t),
                titleName: string.Format("{0}_IterationNumber", this.Name)));
            //返回数据
            return data;
        }

        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="chart"></param>
        public void AddLine(Chart chart, bool isTime)
        {
            //数据描述
            var descp = isTime ? "TimeCost" : "IterationNumber";
            //绘图
            chart.AddSeries(this.Name, SeriesChartType.Line);
            chart.Series[this.Name].Points.DataBindXY(this.infoList.GetPropertyList("Time").ConvertAll(t => (float)t),
                this.infoList.GetPropertyList(descp).ConvertAll(t => (int)t));
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="filePath"></param>
        public Algorithm(string filePath, string Name, float dt = 0.2F)
        {
            //初始化
            this.infoList = new List<AlgorithmBasic>();
            //获得名称
            this.Name = Name;
            //读取所有讯息
            var txtList = File.ReadAllLines(filePath).ToList();
            //时刻
            float time = dt;
            //迭代次数
            int Inumber = new int();
            //遍历所有讯息
            txtList.ForEach(txt =>
            {
                //对语句进行拆分
                var strList = WilsonHelper.SplitStr(txt, new char[2] { ' ', ':' });
                //判断语句类型
                if (strList.First().Contains("CTestNormDispIncr"))
                    Inumber += Convert.ToInt32(strList[4]);
                else if (strList.First().Contains("after"))
                    Inumber += Convert.ToInt32(strList[1]);
                else if (strList[0].Contains("*") && strList.Last().Contains("seconds."))
                {
                    //添加值
                    this.infoList.Add(new AlgorithmBasic
                    {
                        IterationNumber = Inumber,
                        Time = time,
                        TimeCost = Convert.ToInt32(strList[5])
                    });
                    //初始化
                    Inumber = 0; time += dt;
                }
            });
        }
    }

    /// <summary>
    /// 迭代方法集合
    /// </summary>
    public class AlgorithmSet
    {
        /// <summary>
        /// 迭代方法列表
        /// </summary>
        public List<Algorithm> AlgorithmList { set; get; }

        /// <summary>
        /// 获得数据
        /// </summary>
        public GridViewDatas Data
        {
            get
            {
                //初始化
                var data = new GridViewDatas();
                //遍历迭代方法
                this.AlgorithmList.ForEach(algo => data.AddRange(algo.GetData()));
                //返回数据
                return data;
            }
        }

        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="chart"></param>
        public void AddLine(Chart chart, bool isTime)
        {
            chart.Series.Clear();
            this.AlgorithmList.ForEach(algo => algo.AddLine(chart, isTime));
        }

        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="Name"></param>
        /// <returns></returns>
        public bool Add(string filePath, string Name)
        {
            //获得名称列表
            var nameList = this.AlgorithmList.GetPropertyList("Name").ConvertAll(s => (string)s);
            //判断名称是否存在
            if (nameList.Contains(Name)) return MessageBoxExtern.Error("名称已存在");
            //添加数据
            this.AlgorithmList.Add(new NewtonRaphson.Algorithm(filePath, Name));
            return true;
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        public AlgorithmSet()
        {
            this.AlgorithmList = new List<Algorithm>();
        }
    }

    /// <summary>
    /// 基础信息
    /// </summary>
    public class AlgorithmBasic
    {
        /// <summary>
        /// 时刻
        /// </summary>
        public float Time { set; get; }

        /// <summary>
        /// 迭代次数
        /// </summary>
        public int IterationNumber { set; get; }

        /// <summary>
        /// 耗时
        /// </summary>
        public int TimeCost { set; get; }
    }
}
