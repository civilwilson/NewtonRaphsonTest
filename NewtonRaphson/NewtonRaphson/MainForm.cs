﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using PGMHelper;
using System.IO;

namespace NewtonRaphson
{
    public partial class MainForm : Form
    {
        /// <summary>
        /// 算法结果列表
        /// </summary>
        private AlgorithmSet AlgoSet { set; get; }

        /// <summary>
        /// 构造函数
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
            //初始化
            this.AlgoSet = new NewtonRaphson.AlgorithmSet();
            //设置外观
            this.BasicChart.SetApperance(StringAlignment.Near, true);
            this.DataGridView.SetApperance(DataGridViewSelectionMode.ColumnHeaderSelect);
        }

        /// <summary>
        /// 导入结果
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TestButton_Click(object sender, EventArgs e)
        {
            //初始化文件路径
            string filePath = string.Empty;
            //选择文件
            if (!WilsonHelper.OpenFile("文本文件(*.txt)|*.txt|所有文件(*.*)|*.*", ref filePath))
                return;
            //构造对象
            this.AlgoSet.Add(filePath, Path.GetFileNameWithoutExtension(filePath));
            //绘图
            this.AlgoSet.AddLine(this.BasicChart, true);
            this.DataGridView.AddDatas(this.AlgoSet.Data);

        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            this.AlgoSet = new NewtonRaphson.AlgorithmSet();
        }
    }
}
