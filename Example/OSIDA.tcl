#Model Descp
set Model_Descp "OpenSEES"

#Period: Zero will Calculate by OpenSEES
set Period_1 1.33
set Period_2 1.21
#Damping Ratio
set Damping 0.05

#Roof Mass Point
set TopNodeTag 438

#Ground Motion Descp
set GM_Name "GM1"
#Digital Interval
set dt 0.02
#Only Analysis X_Dir
set BothSide "false"

#Min PGA Factor
set IDA_Min 0.2
#Max PGA Factor
set IDA_Max 0.2
#Delta PGA
set delta_IDA 0.1

#Input TCL
source "Model\\12_OSM.tcl"

#Record running script
set fileName "logs\\$GM_Name"
set curtime [clock seconds]
set Time [clock format $curtime -format {%D %T}]
append fileName "_$Time.txt"
set fileName [ string map { "/" "_" " " "_" ":" "_" } $fileName ]
file mkdir "logs"
logFile $fileName

#Clock
set startT [clock seconds]
#IDA Analysis
IDA_Analyze $Period_1 $Period_2 $Damping $GM_Name $dt $IDA_Min $IDA_Max $delta_IDA "true" $Model_Descp $TopNodeTag
if { $BothSide } {
	IDA_Analyze $Period_1 $Period_2 $Damping $GM_Name $dt $IDA_Min $IDA_Max $delta_IDA "false" $Model_Descp $TopNodeTag
}
#EndClock
set endT [clock seconds]
#Puts Time Cost
puts "Time Cost: [expr $endT - $startT] seconds."




