#---------------------------------------------------------
proc Mode_Proc { ModeNum } {
	set pi 3.1415926
	set lambda [eigen  $ModeNum]
	set period "Periods.txt"
	set Periods [open $period "w"]
	set i 1
	foreach lam $lambda {
		set period [expr 2*$pi / sqrt($lam)]
		set period [format "%.3f" $period]
		set str "Mode "; append str $i; append str ": "; append str $period
		puts $Periods "$str"; puts "$str"
		incr i
	}
	close $Periods
	record
}

#---------------------------------------------------------
proc GetMode { ModeID } {
	#Set PI
	set pi 3.1415926
	#Eigen Analysis
	set lambda [eigen  $ModeID]
	#Lambda Vaule
	set lambdai [lindex $lambda [expr $ModeID - 1]]
	#Period Value
	set Period [expr 2 * $pi / sqrt($lambdai)]
	return [format "%.2f" $Period]
}

#---------------------------------------------------------
proc Gravity_Proc { Step } {
	set incr [expr 1./$Step]	
	constraints Transformation
	numberer RCM
	system UmfPack
	test EnergyIncr 1.0e-6 200
	integrator LoadControl $incr
	algorithm Newton
	analysis Static
	analyze $Step
	puts "Gravity Done."
	loadConst -time 0.0
}

#---------------------------------------------------------
proc Rayleigh_Proc { Mode1 Mode2 Damp } {
	set lambdaN [eigen  [expr $Mode2]]
	set lambdaI [lindex $lambdaN [expr $Mode1-1]]
	set lambdaJ [lindex $lambdaN [expr $Mode2-1]]
	set omegaI [expr pow($lambdaI,0.5)]
	set omegaJ [expr pow($lambdaJ,0.5)]
	set alphaM [expr $Damp*(2*$omegaI*$omegaJ)/($omegaI+$omegaJ)]
	set betaKcurr [expr 2.*$Damp/($omegaI+$omegaJ)]
	rayleigh $alphaM $betaKcurr 0 0
	puts "Rayleigh Damping Defined"
}

#---------------------------------------------------------
proc MRayleigh_Proc { Mode1 Mode2 Damp } {
	set pi 3.1415926
	set omegaI [expr 2*$pi / $Mode1]
	set omegaJ [expr 2*$pi / $Mode2]
	set alphaM [expr $Damp*(2*$omegaI*$omegaJ)/($omegaI+$omegaJ)]
	set betaKcurr [expr 2.*$Damp/($omegaI+$omegaJ)]
	rayleigh $alphaM $betaKcurr 0 0
	puts "Rayleigh Damping Done: Period_1 $Mode1, Period_2 $Mode2."
}

#---------------------------------------------------------
proc GMDefine_Proc { LoadTag GMFileList GMDirList GMFactList dt } {
	foreach GMFile $GMFileList GMDir $GMDirList GMFact $GMFactList {
		#File Exist Then Continue (Ensure All)
		if { [file exists $GMFile] == 0} {
			puts "Error: $GMFile doesn't Existed!"
			return -1
		}
		#Define Series
		set AccelSeries "Series -dt $dt -filePath $GMFile -factor $GMFact"
		#Define Load Case
		pattern UniformExcitation $LoadTag $GMDir -accel "Series -dt $dt -filePath $GMFile -factor $GMFact"
		incr LoadTag
	}
	return 0
}

#---------------------------------------------------------
proc GMDAnalyze_Proc { Duration dt { Name "" } } {
	constraints Transformation
	numberer RCM
	system UmfPack
	algorithm Newton
	integrator Newmark 0.55 0.2765625 
	analysis Transient
	set Steps [expr int($Duration / $dt)]
	set Process 10
	set subStartT [clock seconds]
	puts "*$Name 0%"
	for { set step 1 } { $step <= $Steps } { incr step } {
		set ok [ GMDVariableAnalyze $dt ]
		if {$ok != 0} {
			return -1
		}
		if { $step >= $Process} {
			#EndClock
			set subEndT [clock seconds]
			#Puts Time Cost
			puts "*$Name $Process Steps: [expr $subEndT - $subStartT] seconds."
			#New
			set subStartT [clock seconds]
			set Process [expr $Process + 10]
		}
	}
	return $Duration
}


#---------------------------------------------------------
proc GMDVariableAnalyze { dtMax { numSublevels 50 } { iterMin 10 } } {
	#Current Level
	set CurLevel 1
	#Accumulate time
	set AccTime 0
	#Unitl Time Beyond dt
	while { $AccTime < $dtMax } {
		#Analysis
		set ok [Analyze_SingleStep_Proc [expr $dtMax / $CurLevel] [expr $iterMin * $CurLevel]]
		#Success or not
		if { $ok == 0 } {
			#Get Accumulate Analysis Time
			set AccTime [expr $AccTime + [expr $dtMax / $CurLevel ]]
		} else {
			#SubLevel Analysis
			incr CurLevel
			puts "Try SubLevel: $CurLevel"
			#Check Level OutRange
			if { $CurLevel > $numSublevels } {
				return -1
			}
		}
	}
	return 0
}

#---------------------------------------------------------
proc GetFileMaxAbsValue { GMFile } {
	#using Math.h
	namespace import ::tcl::mathfunc::*
	#File Exist Then Continue (Ensure All)
	if { [file exists $GMFile] == 0} {
			puts "Error: $GMFile doesn't Existed!"
			return -1
	} else {
		#initial value
		set max_abs_value 0
		#Read File
		set fp [open $GMFile r]
		#Read line by line
		while { [gets $fp data] >= 0 } {
			set value [abs $data]
			set max_abs_value [get_Max $value $max_abs_value]
		}
		#Close File
		close $fp
		#RETURN
		return $max_abs_value
	}
}

## Function to Get the Bigger Value
proc get_Max {value_1 value_2} {
    if { $value_1 > $value_2 } {
        return $value_1
    } else {
        return $value_2
    }
}

#---------------------------------------------------------
proc GetFilePrtNum { GMFile } {
	#File Exist Then Continue (Ensure All)
	if { [file exists $GMFile] == 0} {
			puts "Error: $GMFile doesn't Existed!"
			return -1
	} else {
		#initial value
		set prtnum 0
		#Read File
		set fp [open $GMFile r]
		#Read line by line
		while { [gets $fp data] >= 0 } {
			incr prtnum
		}
		#Close File
		close $fp
		#RETURN
		return $prtnum
	}
}

#---------------------------------------------------------
proc Analyze_SingleStep_Proc { { dt "" }  { iter 200 } } {
	test NormDispIncr 0.05 $iter 2
	set ok [analyze 1 $dt]
	if { $ok == 0 } {
		return 0
	} else {
		return -1
	}
	
}










