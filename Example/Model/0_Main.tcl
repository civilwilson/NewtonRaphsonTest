wipe
model basic -ndm 3 -ndf 6
source "Model//1_Function.tcl"
source "Model//2_SectionMesh.tcl"
source "Model//3_Node.tcl"
source "Model//4_Constrain.tcl"
source "Model//5_Material.tcl"
source "Model//6_Section.tcl"
source "Model//7_Transf.tcl"
source "Model//8_Element.tcl"
source "Model//9_Region.tcl"
source "Model//10_Load.tcl"
source "Model//11_Recorder.tcl"


