#Elastic Section: 1-T1-250X500
Elastic_Section_Proc_1 1 250 500 45 
#Elastic Section: 2-T1-250X700
Elastic_Section_Proc_1 2 250 700 45 
#Elastic Section: 3-T1-250X600
Elastic_Section_Proc_1 3 250 600 45 
#Elastic Section: 4-T1-300X700
Elastic_Section_Proc_1 4 300 700 45 
#Elastic Section: 5-T1-300X600
Elastic_Section_Proc_1 5 300 600 45 
#Elastic Section: 6-T1-200X500
Elastic_Section_Proc_1 6 200 500 45 
#Elastic Section: 7-T1-200X400
Elastic_Section_Proc_1 7 200 400 45 
#Elastic Section: 8-T1-300X300
Elastic_Section_Proc_1 8 300 300 45 
#Elastic Section: 9-T1-300X500
Elastic_Section_Proc_1 9 300 500 45 
#Elastic Section: 10-T1-200X420
Elastic_Section_Proc_1 10 200 420 45 
#Elastic Section: 11-T1-250X420
Elastic_Section_Proc_1 11 250 420 45 
#Elastic Section: 12-T1-250X620
Elastic_Section_Proc_1 12 250 620 45 
#Elastic Section: 13-T1-250X500
Elastic_Section_Proc_1 13 250 500 45 
#Elastic Section: 14-T1-250X700
Elastic_Section_Proc_1 14 250 700 45 
#Elastic Section: 15-T1-300X700
Elastic_Section_Proc_1 15 300 700 45 
#Elastic Section: 16-T1-300X500
Elastic_Section_Proc_1 16 300 500 45 
#Elastic Section: 17-T1-200X500
Elastic_Section_Proc_1 17 200 500 45 
#Elastic Section: 18-T1-200X400
Elastic_Section_Proc_1 18 200 400 45 
#Elastic Section: 19-T1-Rigid-Beam
Elastic_Section_Proc_1 19 1998 1999 45 
#Fiber Section: 1-T1-250X500-1250-750
Beam_Fiber_Section_Proc_1 20 250 500 20 1250 750 2 2 19 28 
#Fiber Section: 2-T1-250X500-1050-800
Beam_Fiber_Section_Proc_1 21 250 500 20 1050 800 2 2 19 28 
#Fiber Section: 3-T1-250X500-1000-800
Beam_Fiber_Section_Proc_1 22 250 500 20 1000 800 2 2 19 28 
#Fiber Section: 4-T1-250X700-1450-800
Beam_Fiber_Section_Proc_1 23 250 700 20 1450 800 2 2 20 28 
#Fiber Section: 5-T1-250X700-950-700
Beam_Fiber_Section_Proc_1 24 250 700 20 950 700 2 2 20 28 
#Fiber Section: 6-T1-250X700-1000-700
Beam_Fiber_Section_Proc_1 25 250 700 20 1000 700 2 2 20 28 
#Fiber Section: 7-T1-250X500-1550-850
Beam_Fiber_Section_Proc_1 26 250 500 20 1550 850 2 2 19 28 
#Fiber Section: 8-T1-250X500-1250-900
Beam_Fiber_Section_Proc_1 27 250 500 20 1250 900 2 2 19 28 
#Fiber Section: 9-T1-250X500-1350-1100
Beam_Fiber_Section_Proc_1 28 250 500 20 1350 1100 2 2 19 28 
#Fiber Section: 10-T1-250X700-1650-900
Beam_Fiber_Section_Proc_1 29 250 700 20 1650 900 2 2 20 28 
#Fiber Section: 11-T1-250X700-1050-700
Beam_Fiber_Section_Proc_1 30 250 700 20 1050 700 2 2 20 28 
#Fiber Section: 12-T1-250X700-1150-700
Beam_Fiber_Section_Proc_1 31 250 700 20 1150 700 2 2 20 28 
#Fiber Section: 13-T1-250X500-1250-650
Beam_Fiber_Section_Proc_1 32 250 500 20 1250 650 2 2 19 28 
#Fiber Section: 14-T1-250X500-1050-700
Beam_Fiber_Section_Proc_1 33 250 500 20 1050 700 2 2 19 28 
#Fiber Section: 15-T1-250X500-1000-700
Beam_Fiber_Section_Proc_1 34 250 500 20 1000 700 2 2 19 28 
#Fiber Section: 16-T1-250X700-700-700
Beam_Fiber_Section_Proc_1 35 250 700 20 700 700 2 2 20 28 
#Fiber Section: 17-T1-250X700-900-700
Beam_Fiber_Section_Proc_1 36 250 700 20 900 700 2 2 20 28 
#Fiber Section: 18-T1-250X500-1200-650
Beam_Fiber_Section_Proc_1 37 250 500 20 1200 650 2 2 19 28 
#Fiber Section: 19-T1-250X500-1000-750
Beam_Fiber_Section_Proc_1 38 250 500 20 1000 750 2 2 19 28 
#Fiber Section: 20-T1-250X500-1050-850
Beam_Fiber_Section_Proc_1 39 250 500 20 1050 850 2 2 19 28 
#Fiber Section: 21-T1-250X500-1100-600
Beam_Fiber_Section_Proc_1 40 250 500 20 1100 600 2 2 19 28 
#Fiber Section: 22-T1-250X500-950-650
Beam_Fiber_Section_Proc_1 41 250 500 20 950 650 2 2 19 28 
#Fiber Section: 23-T1-250X500-1050-750
Beam_Fiber_Section_Proc_1 42 250 500 20 1050 750 2 2 19 28 
#Fiber Section: 24-T1-250X500-1350-750
Beam_Fiber_Section_Proc_1 43 250 500 20 1350 750 2 2 19 28 
#Fiber Section: 25-T1-250X500-900-800
Beam_Fiber_Section_Proc_1 44 250 500 20 900 800 2 2 19 28 
#Fiber Section: 26-T1-250X500-1400-800
Beam_Fiber_Section_Proc_1 45 250 500 20 1400 800 2 2 19 28 
#Fiber Section: 27-T1-250X600-1900-300
Beam_Fiber_Section_Proc_1 46 250 600 20 1900 300 2 2 18 28 
#Fiber Section: 28-T1-300X700-1500-1250
Beam_Fiber_Section_Proc_1 47 300 700 20 1500 1250 3 3 19 28 
#Fiber Section: 29-T1-300X700-1250-1300
Beam_Fiber_Section_Proc_1 48 300 700 20 1250 1300 3 3 19 28 
#Fiber Section: 30-T1-300X700-1750-1150
Beam_Fiber_Section_Proc_1 49 300 700 20 1750 1150 3 3 19 28 
#Fiber Section: 31-T1-300X600-1500-400
Beam_Fiber_Section_Proc_1 50 300 600 20 1500 400 3 3 17 28 
#Fiber Section: 32-T1-200X500-400-400
Beam_Fiber_Section_Proc_1 51 200 500 20 400 400 2 2 19 28 
#Fiber Section: 33-T1-200X500-600-400
Beam_Fiber_Section_Proc_1 52 200 500 20 600 400 2 2 19 28 
#Fiber Section: 34-T1-200X500-450-200
Beam_Fiber_Section_Proc_1 53 200 500 20 450 200 2 2 18 28 
#Fiber Section: 35-T1-250X700-900-800
Beam_Fiber_Section_Proc_1 54 250 700 20 900 800 2 2 20 28 
#Fiber Section: 36-T1-250X700-800-700
Beam_Fiber_Section_Proc_1 55 250 700 20 800 700 2 2 20 28 
#Fiber Section: 37-T1-250X700-850-700
Beam_Fiber_Section_Proc_1 56 250 700 20 850 700 2 2 20 28 
#Fiber Section: 38-T1-250X700-1100-700
Beam_Fiber_Section_Proc_1 57 250 700 20 1100 700 2 2 20 28 
#Fiber Section: 39-T1-250X700-950-800
Beam_Fiber_Section_Proc_1 58 250 700 20 950 800 2 2 20 28 
#Fiber Section: 40-T1-250X700-1200-700
Beam_Fiber_Section_Proc_1 59 250 700 20 1200 700 2 2 20 28 
#Fiber Section: 41-T1-200X400-550-350
Beam_Fiber_Section_Proc_1 60 200 400 20 550 350 2 2 19 28 
#Fiber Section: 42-T1-200X400-500-350
Beam_Fiber_Section_Proc_1 61 200 400 20 500 350 2 2 19 28 
#Fiber Section: 43-T1-250X700-950-850
Beam_Fiber_Section_Proc_1 62 250 700 20 950 850 2 2 20 28 
#Fiber Section: 44-T1-200X400-600-350
Beam_Fiber_Section_Proc_1 63 200 400 20 600 350 2 2 19 28 
#Fiber Section: 45-T1-250X700-1600-850
Beam_Fiber_Section_Proc_1 64 250 700 20 1600 850 2 2 20 28 
#Fiber Section: 46-T1-250X700-1250-1050
Beam_Fiber_Section_Proc_1 65 250 700 20 1250 1050 2 2 20 28 
#Fiber Section: 47-T1-250X700-1150-750
Beam_Fiber_Section_Proc_1 66 250 700 20 1150 750 2 2 20 28 
#Fiber Section: 48-T1-200X500-850-450
Beam_Fiber_Section_Proc_1 67 200 500 20 850 450 2 2 19 28 
#Fiber Section: 49-T1-250X700-1350-750
Beam_Fiber_Section_Proc_1 68 250 700 20 1350 750 2 2 20 28 
#Fiber Section: 50-T1-250X700-1250-1150
Beam_Fiber_Section_Proc_1 69 250 700 20 1250 1150 2 2 20 28 
#Fiber Section: 51-T1-250X700-1100-950
Beam_Fiber_Section_Proc_1 70 250 700 20 1100 950 2 2 20 28 
#Fiber Section: 52-T1-250X700-1300-1250
Beam_Fiber_Section_Proc_1 71 250 700 20 1300 1250 2 2 20 28 
#Fiber Section: 53-T1-250X700-1050-1000
Beam_Fiber_Section_Proc_1 72 250 700 20 1050 1000 2 2 20 28 
#Fiber Section: 54-T1-200X500-700-400
Beam_Fiber_Section_Proc_1 73 200 500 20 700 400 2 2 19 28 
#Fiber Section: 55-T1-250X700-1400-750
Beam_Fiber_Section_Proc_1 74 250 700 20 1400 750 2 2 20 28 
#Fiber Section: 56-T1-250X700-1600-900
Beam_Fiber_Section_Proc_1 75 250 700 20 1600 900 2 2 20 28 
#Fiber Section: 57-T1-300X700-1650-900
Beam_Fiber_Section_Proc_1 76 300 700 20 1650 900 3 3 19 28 
#Fiber Section: 58-T1-250X500-1450-800
Beam_Fiber_Section_Proc_1 77 250 500 20 1450 800 2 2 19 28 
#Fiber Section: 59-T1-250X500-1150-600
Beam_Fiber_Section_Proc_1 78 250 500 20 1150 600 2 2 19 28 
#Fiber Section: 60-T1-200X500-500-500
Beam_Fiber_Section_Proc_1 79 200 500 20 500 500 2 2 19 28 
#Fiber Section: 61-T1-200X500-600-450
Beam_Fiber_Section_Proc_1 80 200 500 20 600 450 2 2 19 28 
#Fiber Section: 62-T1-250X500-1050-900
Beam_Fiber_Section_Proc_1 81 250 500 20 1050 900 2 2 19 28 
#Fiber Section: 63-T1-250X500-1100-900
Beam_Fiber_Section_Proc_1 82 250 500 20 1100 900 2 2 19 28 
#Fiber Section: 64-T1-250X500-800-800
Beam_Fiber_Section_Proc_1 83 250 500 20 800 800 2 2 19 28 
#Fiber Section: 65-T1-250X500-800-650
Beam_Fiber_Section_Proc_1 84 250 500 20 800 650 2 2 19 28 
#Fiber Section: 66-T1-250X500-1000-500
Beam_Fiber_Section_Proc_1 85 250 500 20 1000 500 2 2 19 28 
#Fiber Section: 67-T1-250X500-700-550
Beam_Fiber_Section_Proc_1 86 250 500 20 700 550 2 2 19 28 
#Fiber Section: 68-T1-250X500-800-500
Beam_Fiber_Section_Proc_1 87 250 500 20 800 500 2 2 19 28 
#Fiber Section: 69-T1-300X700-1450-850
Beam_Fiber_Section_Proc_1 88 300 700 20 1450 850 3 3 19 28 
#Fiber Section: 70-T1-250X500-750-650
Beam_Fiber_Section_Proc_1 89 250 500 20 750 650 2 2 19 28 
#Fiber Section: 71-T1-250X500-1000-650
Beam_Fiber_Section_Proc_1 90 250 500 20 1000 650 2 2 19 28 
#Fiber Section: 72-T1-250X700-1350-700
Beam_Fiber_Section_Proc_1 91 250 700 20 1350 700 2 2 20 28 
#Fiber Section: 73-T1-300X700-1550-850
Beam_Fiber_Section_Proc_1 92 300 700 20 1550 850 3 3 19 28 
#Fiber Section: 74-T1-250X500-1050-550
Beam_Fiber_Section_Proc_1 93 250 500 20 1050 550 2 2 19 28 
#Fiber Section: 75-T1-250X500-600-600
Beam_Fiber_Section_Proc_1 94 250 500 20 600 600 2 2 19 28 
#Fiber Section: 76-T1-250X500-850-500
Beam_Fiber_Section_Proc_1 95 250 500 20 850 500 2 2 19 28 
#Fiber Section: 77-T1-250X500-550-600
Beam_Fiber_Section_Proc_1 96 250 500 20 550 600 2 2 19 28 
#Fiber Section: 78-T1-250X500-900-500
Beam_Fiber_Section_Proc_1 97 250 500 20 900 500 2 2 19 28 
#Fiber Section: 79-T1-250X500-950-500
Beam_Fiber_Section_Proc_1 98 250 500 20 950 500 2 2 19 28 
#Fiber Section: 80-T1-250X500-550-550
Beam_Fiber_Section_Proc_1 99 250 500 20 550 550 2 2 19 28 
#Fiber Section: 81-T1-250X500-550-650
Beam_Fiber_Section_Proc_1 100 250 500 20 550 650 2 2 19 28 
#Fiber Section: 82-T1-300X700-1150-900
Beam_Fiber_Section_Proc_1 101 300 700 20 1150 900 3 3 19 28 
#Fiber Section: 83-T1-300X700-850-1000
Beam_Fiber_Section_Proc_1 102 300 700 20 850 1000 3 3 19 28 
#Fiber Section: 84-T1-300X700-1350-850
Beam_Fiber_Section_Proc_1 103 300 700 20 1350 850 3 3 19 28 
#Fiber Section: 85-T1-200X500-500-400
Beam_Fiber_Section_Proc_1 104 200 500 20 500 400 2 2 19 28 
#Fiber Section: 86-T1-200X500-400-200
Beam_Fiber_Section_Proc_1 105 200 500 20 400 200 2 2 18 28 
#Fiber Section: 87-T1-250X700-750-700
Beam_Fiber_Section_Proc_1 106 250 700 20 750 700 2 2 20 28 
#Fiber Section: 88-T1-200X400-400-350
Beam_Fiber_Section_Proc_1 107 200 400 20 400 350 2 2 19 28 
#Fiber Section: 89-T1-200X400-450-350
Beam_Fiber_Section_Proc_1 108 200 400 20 450 350 2 2 19 28 
#Fiber Section: 90-T1-250X700-1250-700
Beam_Fiber_Section_Proc_1 109 250 700 20 1250 700 2 2 20 28 
#Fiber Section: 91-T1-250X700-750-750
Beam_Fiber_Section_Proc_1 110 250 700 20 750 750 2 2 20 28 
#Fiber Section: 92-T1-200X500-750-400
Beam_Fiber_Section_Proc_1 111 200 500 20 750 400 2 2 19 28 
#Fiber Section: 93-T1-250X700-700-950
Beam_Fiber_Section_Proc_1 112 250 700 20 700 950 2 2 20 28 
#Fiber Section: 94-T1-250X700-1300-700
Beam_Fiber_Section_Proc_1 113 250 700 20 1300 700 2 2 20 28 
#Fiber Section: 95-T1-250X700-800-800
Beam_Fiber_Section_Proc_1 114 250 700 20 800 800 2 2 20 28 
#Fiber Section: 96-T1-300X700-1300-850
Beam_Fiber_Section_Proc_1 115 300 700 20 1300 850 3 3 19 28 
#Fiber Section: 97-T1-300X700-1800-950
Beam_Fiber_Section_Proc_1 116 300 700 20 1800 950 3 3 19 28 
#Fiber Section: 98-T1-250X500-1000-550
Beam_Fiber_Section_Proc_1 117 250 500 20 1000 550 2 2 19 28 
#Fiber Section: 99-T1-200X500-400-450
Beam_Fiber_Section_Proc_1 118 200 500 20 400 450 2 2 19 28 
#Fiber Section: 100-T1-250X500-800-550
Beam_Fiber_Section_Proc_1 119 250 500 20 800 550 2 2 19 28 
#Fiber Section: 101-T1-250X500-650-700
Beam_Fiber_Section_Proc_1 120 250 500 20 650 700 2 2 19 28 
#Fiber Section: 102-T1-250X500-500-600
Beam_Fiber_Section_Proc_1 121 250 500 20 500 600 2 2 19 28 
#Fiber Section: 103-T1-250X500-550-500
Beam_Fiber_Section_Proc_1 122 250 500 20 550 500 2 2 19 28 
#Fiber Section: 104-T1-250X500-700-500
Beam_Fiber_Section_Proc_1 123 250 500 20 700 500 2 2 19 28 
#Fiber Section: 105-T1-250X500-600-500
Beam_Fiber_Section_Proc_1 124 250 500 20 600 500 2 2 19 28 
#Fiber Section: 106-T1-250X500-500-500
Beam_Fiber_Section_Proc_1 125 250 500 20 500 500 2 2 19 28 
#Fiber Section: 107-T1-250X500-750-500
Beam_Fiber_Section_Proc_1 126 250 500 20 750 500 2 2 19 28 
#Fiber Section: 108-T1-250X600-1950-300
Beam_Fiber_Section_Proc_1 127 250 600 20 1950 300 2 2 18 28 
#Fiber Section: 109-T1-300X700-950-850
Beam_Fiber_Section_Proc_1 128 300 700 20 950 850 3 3 19 28 
#Fiber Section: 110-T1-300X700-850-850
Beam_Fiber_Section_Proc_1 129 300 700 20 850 850 3 3 19 28 
#Fiber Section: 111-T1-300X700-1150-850
Beam_Fiber_Section_Proc_1 130 300 700 20 1150 850 3 3 19 28 
#Fiber Section: 112-T1-200X500-450-400
Beam_Fiber_Section_Proc_1 131 200 500 20 450 400 2 2 19 28 
#Fiber Section: 113-T1-200X500-400-500
Beam_Fiber_Section_Proc_1 132 200 500 20 400 500 2 2 19 28 
#Fiber Section: 114-T1-250X500-650-500
Beam_Fiber_Section_Proc_1 133 250 500 20 650 500 2 2 19 28 
#Fiber Section: 115-T1-200X400-350-350
Beam_Fiber_Section_Proc_1 134 200 400 20 350 350 2 2 19 28 
#Fiber Section: 116-T1-300X700-1000-850
Beam_Fiber_Section_Proc_1 135 300 700 20 1000 850 3 3 19 28 
#Fiber Section: 117-T1-250X420-450-450
Beam_Fiber_Section_Proc_1 136 250 420 20 450 450 2 2 19 28 
#Fiber Section: 118-T1-250X620-900-650
Beam_Fiber_Section_Proc_1 137 250 620 20 900 650 2 2 20 28 
#Fiber Section: 119-T1-250X500-500-500
Beam_Fiber_Section_Proc_1 138 250 500 25 500 500 2 2 19 28 
#Fiber Section: 120-T1-250X700-700-700
Beam_Fiber_Section_Proc_1 139 250 700 25 700 700 2 2 20 28 
#Fiber Section: 121-T1-250X700-950-700
Beam_Fiber_Section_Proc_1 140 250 700 25 950 700 2 2 20 28 
#Fiber Section: 122-T1-250X500-550-500
Beam_Fiber_Section_Proc_1 141 250 500 25 550 500 2 2 19 28 
#Fiber Section: 123-T1-250X700-900-700
Beam_Fiber_Section_Proc_1 142 250 700 25 900 700 2 2 20 28 
#Fiber Section: 124-T1-250X700-1000-700
Beam_Fiber_Section_Proc_1 143 250 700 25 1000 700 2 2 20 28 
#Fiber Section: 125-T1-250X500-650-500
Beam_Fiber_Section_Proc_1 144 250 500 25 650 500 2 2 19 28 
#Fiber Section: 126-T1-250X500-1050-550
Beam_Fiber_Section_Proc_1 145 250 500 25 1050 550 2 2 19 28 
#Fiber Section: 127-T1-250X500-2600-1550
Beam_Fiber_Section_Proc_1 146 250 500 25 2600 1550 2 2 19 28 
#Fiber Section: 128-T1-300X700-850-850
Beam_Fiber_Section_Proc_1 147 300 700 25 850 850 3 3 19 28 
#Fiber Section: 129-T1-300X500-2150-1300
Beam_Fiber_Section_Proc_1 148 300 500 25 2150 1300 3 3 19 28 
#Fiber Section: 130-T1-200X500-400-400
Beam_Fiber_Section_Proc_1 149 200 500 25 400 400 2 2 19 28 
#Fiber Section: 131-T1-200X500-400-200
Beam_Fiber_Section_Proc_1 150 200 500 25 400 200 2 2 18 28 
#Fiber Section: 132-T1-200X400-350-350
Beam_Fiber_Section_Proc_1 151 200 400 25 350 350 2 2 19 28 
#Fiber Section: 133-T1-250X700-800-700
Beam_Fiber_Section_Proc_1 152 250 700 25 800 700 2 2 20 28 
#Fiber Section: 134-T1-200X500-850-200
Beam_Fiber_Section_Proc_1 153 200 500 25 850 200 2 2 18 28 
#Fiber Section: 135-T1-200X500-200-200
Beam_Fiber_Section_Proc_1 154 200 500 25 200 200 2 2 18 28 
#Fiber Section: 136-T1-250X500-600-500
Beam_Fiber_Section_Proc_1 155 250 500 25 600 500 2 2 19 28 
#Fiber Section: 137-T1-200X500-450-400
Beam_Fiber_Section_Proc_1 156 200 500 25 450 400 2 2 19 28 
#Fiber Section: 138-T1-250X700-750-700
Beam_Fiber_Section_Proc_1 157 250 700 25 750 700 2 2 20 28 
#Fiber Section: 139-T1-250X700-850-700
Beam_Fiber_Section_Proc_1 158 250 700 25 850 700 2 2 20 28 
#Fiber Section: 1-T1-500X500-1000-1000
Fiber_Section_Proc_1 159 500 500 20 250 500 500 2 2 21 28 
#Fiber Section: 2-T1-500X500-900-900
Fiber_Section_Proc_1 160 500 500 20 250 400 400 2 2 24 28 
#Fiber Section: 3-T1-500X700-1050-1350
Fiber_Section_Proc_1 161 500 700 20 300 450 750 2 3 24 28 
#Fiber Section: 4-T1-500X800-1350-1650
Fiber_Section_Proc_1 162 500 800 20 350 650 950 2 4 24 28 
#Fiber Section: 5-T1-500X500-1250-950
Fiber_Section_Proc_1 163 500 500 20 250 750 450 2 2 21 28 
#Fiber Section: 6-T1-500X500-900-900
Fiber_Section_Proc_1 164 500 500 20 250 400 400 2 2 21 28 
#Fiber Section: 7-T1-500X500-1350-1050
Fiber_Section_Proc_1 165 500 500 20 250 850 550 2 2 21 28 
#Fiber Section: 8-T1-700X500-1650-1050
Fiber_Section_Proc_1 166 700 500 20 300 1050 450 3 2 23 28 
#Fiber Section: 9-T1-700X500-1350-1200
Fiber_Section_Proc_1 167 700 500 20 300 750 600 3 2 23 28 
#Fiber Section: 10-T1-800X500-1800-1450
Fiber_Section_Proc_1 168 800 500 20 350 1100 750 4 2 23 28 
#Fiber Section: 11-T1-800X500-1650-1600
Fiber_Section_Proc_1 169 800 500 20 350 950 900 4 2 23 28 
#Fiber Section: 12-T1-700X500-1350-1050
Fiber_Section_Proc_1 170 700 500 20 300 750 450 3 2 23 28 
#Fiber Section: 13-T1-500X500-1250-1150
Fiber_Section_Proc_1 171 500 500 20 350 550 450 2 2 21 28 
#Fiber Section: 14-T1-400X400-700-700
Fiber_Section_Proc_1 172 400 400 20 250 200 200 1 1 22 28 
#Fiber Section: 15-T1-400X400-700-700
Fiber_Section_Proc_1 173 400 400 20 250 200 200 1 1 24 28 
#Fiber Section: 16-T1-500X800-1150-1650
Fiber_Section_Proc_1 174 500 800 20 350 450 950 2 4 24 28 
#Fiber Section: 17-T1-500X500-900-950
Fiber_Section_Proc_1 175 500 500 20 250 400 450 2 2 21 28 
#Fiber Section: 18-T1-800X500-1650-1150
Fiber_Section_Proc_1 176 800 500 20 350 950 450 4 2 23 28 
#Fiber Section: 19-T1-400X400-600-600
Fiber_Section_Proc_1 177 400 400 20 200 200 200 1 1 22 28 
#Fiber Section: 20-T1-400X400-600-700
Fiber_Section_Proc_1 178 400 400 20 200 200 300 1 1 22 28 
#Fiber Section: 21-T1-400X600-750-1000
Fiber_Section_Proc_1 179 400 600 20 250 250 500 1 3 22 28 
#Fiber Section: 22-T1-400X700-800-1200
Fiber_Section_Proc_1 180 400 700 20 300 200 600 1 3 22 28 
#Fiber Section: 23-T1-400X400-600-650
Fiber_Section_Proc_1 181 400 400 20 200 200 250 1 1 22 28 
#Fiber Section: 24-T1-600X400-1100-750
Fiber_Section_Proc_1 182 600 400 20 250 600 250 3 1 21 28 
#Fiber Section: 25-T1-600X400-1000-750
Fiber_Section_Proc_1 183 600 400 20 250 500 250 3 1 21 28 
#Fiber Section: 26-T1-700X400-1200-800
Fiber_Section_Proc_1 184 700 400 20 300 600 200 3 1 21 28 
#Fiber Section: 27-T1-600X400-1000-750
Fiber_Section_Proc_1 185 600 400 20 250 500 250 3 1 23 28 
#Fiber Section: 28-T1-600X400-1250-750
Fiber_Section_Proc_1 186 600 400 20 250 750 250 3 1 21 28 
#Fiber Section: 29-T1-400X400-600-600
Fiber_Section_Proc_1 187 400 400 20 200 200 200 1 1 16 28 
#Fiber Section: 30-T1-400X400-600-650
Fiber_Section_Proc_1 188 400 400 20 200 200 250 1 1 16 28 
#Fiber Section: 31-T1-400X600-750-1000
Fiber_Section_Proc_1 189 400 600 20 250 250 500 1 3 16 28 
#Fiber Section: 32-T1-400X700-800-1200
Fiber_Section_Proc_1 190 400 700 20 300 200 600 1 3 16 28 
#Fiber Section: 33-T1-400X400-600-700
Fiber_Section_Proc_1 191 400 400 20 200 200 300 1 1 16 28 
#Fiber Section: 34-T1-600X400-1250-750
Fiber_Section_Proc_1 192 600 400 20 250 750 250 3 1 15 28 
#Fiber Section: 35-T1-600X400-1000-750
Fiber_Section_Proc_1 193 600 400 20 250 500 250 3 1 15 28 
#Fiber Section: 36-T1-700X400-1200-800
Fiber_Section_Proc_1 194 700 400 20 300 600 200 3 1 15 28 
