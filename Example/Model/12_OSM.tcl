#--------------------------------------------------------------------------------------
proc GM_Analyze { Period_1 Period_2 Damping GM_Name dt PGA is_X { Model_Descp "" } { nodeTag 0 } } {
	#Input Model
	source "Model\\0_Main.tcl"
	#Damping Ratio Define
	MRayleigh_Proc $Period_1 $Period_2 $Damping
	#Gravity_Analysis
	Gravity_Proc 10
	#Define Recorder
	DefineRecorder $GM_Name $PGA $is_X 
	#Load Case Name
	set lc_name [GetLoadCaseName $GM_Name $is_X ]
	#Ground Motion Analysis
	set TimeCost [GroundMotionAnalysis $GM_Name $PGA $is_X $dt $Model_Descp]
	#Check State
	if { $TimeCost == -1 } {
		return -1
	}
	#Analysis Time
	puts "*$Model_Descp $lc_name $PGA Finish with [format "%.0f" $TimeCost] second."
	#Clear All
	wipe All
	#Return
	return 0
}

#---------------------------------------------------------
#IDA Analysis
proc IDA_Analyze { Period_1 Period_2 Damping GM_Name dt IDA_Min IDA_Max delta_IDA is_X { Model_Descp "" } { nodeTag 0 } } {
	#IDA Analysis
	for { set PGA [expr $IDA_Min] } { $PGA <= $IDA_Max } { set PGA [expr $PGA + $delta_IDA] } {
		#Due With Format
		set PGA [format "%.3f" $PGA]
		#Load Case Name
		set lc_name [GetLoadCaseName $GM_Name $is_X]
		#Run Analysis
		set Success [expr [GM_Analyze $Period_1 $Period_2 $Damping $GM_Name $dt $PGA $is_X $Model_Descp $nodeTag]]
		#Analysis Failed
		if { $Success == -1 } {
			puts "* $Model_Descp $lc_name $PGA FAILED!"
			break
		}
	}
}

#---------------------------------------------------------
#Create GroundMotion Analysis Directory Path
proc DefineRecorder { GM_Name PGA is_X } {
	#Directory_Name
	set direct_name "$GM_Name"
	#Define Direction
	if { $is_X} {
		append direct_name "X\\$PGA"
	} else {
		append direct_name "Y\\$PGA"
	}
	#Create Directory
	file mkdir $direct_name
	#Define Recorder
	Recorder_Proc $direct_name
} 

#---------------------------------------------------------
#Get LoadCase Name Consider Direction
proc GetLoadCaseName { GM_Name is_X } {
	#Directory_Name
	set lc_name "$GM_Name"
	#Define Direction
	if { $is_X} {
		append lc_name "X"
	} else {
		append lc_name "Y"
	}
	#RETURN
	return $lc_name
}

#---------------------------------------------------------
#GroundMotion Analysis
proc GroundMotionAnalysis { GM_Name PGA is_X dt { Model_Descp "" } } {
	#Define Factor
	set Factor_X [expr $is_X ? $PGA * 9800. : $PGA * 9800. * 0.85]
	set Factor_Y [expr $is_X ? $PGA * 9800. * 0.85 : $PGA * 9800.]
	#LoadCaseName
	set lc_name [GetLoadCaseName $GM_Name $is_X]
	#Max Vaule in GM file (abs)
	set MaxAbsValue [ GetFileMaxAbsValue "GroundMotion\\$GM_Name.txt" ]
	#Define FileName_List
	set iGMfileList [list "GroundMotion//$GM_Name.txt" "GroundMotion//$GM_Name.txt"]
	#Define Direction
	set iGMdirList [list "1" "2"] 
	#Define Factors
	set iGMfactList [list [expr $Factor_X / $MaxAbsValue] [expr $Factor_Y / $MaxAbsValue]]
	#Define Ground Motion
	if { [GMDefine_Proc 1001 $iGMfileList $iGMdirList $iGMfactList $dt] == -1} {
		return -1
	}
	#Get File PrtNumber
	set PrtNum [GetFilePrtNum "GroundMotion\\$GM_Name.txt"]
	set Duration [expr $PrtNum * $dt]
	#GMDAnalyze_Proc
	return [GMDAnalyze_Proc $Duration 0.02 "$Model_Descp $lc_name $PGA"]
}


