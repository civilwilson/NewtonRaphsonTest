# Beam Fiber Section CORP YJK Section 1
proc Beam_Fiber_Section_Proc_1 { SecTag Width Height Cover Asup Asdown nup ndown ConTag SteelTag } {
    set HWidth [expr $Width * 0.5]
    set HHeight [expr $Height * 0.5]
    #Define Fiber Section
    section Fiber $SecTag {
        # Concrete
        patch rect $ConTag 4 4 -$HWidth -$HHeight $HWidth $HHeight
        # As 
        Beam_As_Proc_1 $Width $Height $Cover $Asup $Asdown $nup $ndown $SteelTag
    }
}

# Beam As Fiber CORP Rect Section
proc Beam_As_Proc_1 { Width Height Cover Asup Asdown nup ndown SteelTag} {
    set AsPos [expr $Height * 0.5 - $Cover] 
    set AsWPos [expr $Width * 0.5 - $Cover]
    set EachFiberAreaU [expr $Asup /$nup]
    set EachFiberAreaD [expr $Asdown /$ndown]
    # Top As Fiber
    layer straight $SteelTag $nup $EachFiberAreaU -$AsWPos $AsPos  $AsWPos $AsPos 
    # Bottom As Fiber
    layer straight $SteelTag $ndown $EachFiberAreaD -$AsWPos -$AsPos  $AsWPos -$AsPos 
}

# Column Fiber Section CORP YJK Section 1
proc Fiber_Section_Proc_1 { SecTag Width Height Cover As_corner As_mid_x As_mid_y n_mid_x n_mid_y ConTag SteelTag } {
    set HWidth [expr $Width * 0.5]
    set HHeight [expr $Height * 0.5]
    section Fiber $SecTag {
        # Concrete
        patch rect $ConTag 4 4 -$HWidth -$HHeight $HWidth $HHeight
        # As 
        Column_As_Proc_1 $Width $Height $Cover $As_corner $As_mid_x $As_mid_y $n_mid_x $n_mid_y $SteelTag
    }
}

# Column As Fiber CORP Rect Section
proc Column_As_Proc_1 { Width Height Cover As_corner As_mid_x As_mid_y n_mid_x n_mid_y SteelTag} {
    set AsPos [expr $Height * 0.5 - $Cover] 
    set AsWPos [expr $Width * 0.5 - $Cover]
    set EachFiberAreaX [expr $As_mid_x / $n_mid_x]
    set EachFiberAreaY [expr $As_mid_y / $n_mid_y]
    set Space_mid_X [expr ($AsWPos * 2 ) /($n_mid_x + 1)]
    set Space_mid_Y [expr ($AsPos * 2 ) /($n_mid_y + 1)]
    set Start_mid_X [expr -$AsWPos + $Space_mid_X]
    set End_mid_X [expr $AsWPos - $Space_mid_X]
    set Start_mid_Y [expr -$AsPos + $Space_mid_Y]
    set End_mid_Y [expr $AsPos - $Space_mid_Y]    
    # Corner As At Down
    layer straight $SteelTag 2 $As_corner -$AsWPos -$AsPos  $AsWPos -$AsPos 
    # Corner As At Top
    layer straight $SteelTag 2 $As_corner -$AsWPos $AsPos  $AsWPos $AsPos 
    # X Dirct As At Down
    layer straight $SteelTag $n_mid_x $EachFiberAreaX $Start_mid_X -$AsPos  $End_mid_X -$AsPos 
    # X Dirct As At Top
    layer straight $SteelTag $n_mid_x $EachFiberAreaX $Start_mid_X $AsPos  $End_mid_X $AsPos     
    # Y Dirct As At Left   
    layer straight $SteelTag $n_mid_y $EachFiberAreaY -$AsWPos $Start_mid_Y -$AsWPos $End_mid_Y
    # Y Dirct As At Right
    layer straight $SteelTag $n_mid_y $EachFiberAreaY $AsWPos $Start_mid_Y $AsWPos $End_mid_Y
}

# Elasitc Section CORP YJK Section 1
proc Elastic_Section_Proc_1 { SecTag Width Height  ConTag  } {
    set HWidth [expr $Width * 0.5]
    set HHeight [expr $Height * 0.5]
    section Fiber $SecTag {
        patch rect $ConTag 4 4 -$HWidth -$HHeight $HWidth $HHeight
    }
}




