#Tower 1 Storey All: Beam_Disp_Element i_Joint_Element Region
region 1 -ele 1 3 6 8 11 13 16 18 21 23 26 29 32 34 37 39 42 44 47 49 51 54 56 59 61 64 67 70 73 76 79 82 85 88 91 94 97 100 103 106 109 111 114 117 120 122 125 128 131 134 137 140 171 181 186 192 194 197 199 201 203 206 208 210 212 215 218 221 223 226 228 231 233 236 238 240 243 245 248 250 253 256 259 262 265 268 271 274 277 280 283 286 289 292 295 298 300 303 306 309 311 314 317 320 323 325 328 382 392 397 403 406 409 411 413 415 418 420 422 424 427 430 433 435 438 440 443 445 448 450 452 455 457 460 462 465 468 471 474 477 480 483 486 489 492 495 498 501 504 507 510 512 515 518 521 523 526 528 531 534 537 540 572 582 587 593 596 599 601 603 605 608 610 612 614 617 620 623 625 628 630 633 635 638 640 642 645 647 650 652 655 658 661 664 667 670 673 676 679 682 685 688 691 694 697 700 702 705 708 711 713 716 718 721 724 727 730 762 772 777 783 786 789 791 793 795 798 800 802 804 807 810 813 815 818 820 823 825 828 830 832 835 837 840 842 845 848 851 854 857 860 863 866 869 872 875 878 881 884 887 890 892 895 898 901 903 906 908 911 914 917 920 952 962 967 973 976 979 981 983 985 988 990 992 994 997 1000 1003 1005 1008 1010 1013 1015 1018 1020 1022 1025 1027 1030 1032 1035 1038 1041 1044 1047 1050 1053 1056 1059 1062 1065 1068 1071 1074 1076 1079 1081 1084 1087 1090 1092 1095 1097 1100 1103 1106 1109 1143 1153 1158 1168 1171 1174 1176 1178 1180 1183 1185 1187 1189 1192 1195 1198 1200 1203 1205 1208 1210 1213 1215 1217 1220 1222 1225 1227 1230 1233 1236 1239 1242 1245 1248 1251 1254 1257 1260 1263 1266 1269 1271 1274 1276 1279 1282 1285 1288 1291 1294 1297 1300 1325 1333 1339 1342 
#Tower 1 Storey All: Beam_Disp_Element j_Joint_Element Region
region 2 -ele 5 10 15 20 25 28 31 36 41 46 53 58 63 66 69 72 75 78 81 84 87 90 93 96 99 102 105 108 113 116 119 124 130 133 136 139 145 147 149 152 154 157 159 161 163 165 173 175 183 185 188 190 196 205 214 217 220 225 230 235 242 247 252 255 258 261 264 267 270 273 276 279 282 285 288 291 294 297 302 305 308 313 319 322 327 334 336 338 347 349 356 361 363 367 369 371 373 375 384 386 394 396 399 401 405 408 417 426 429 432 437 442 447 454 459 464 467 470 473 476 479 482 485 488 491 494 497 500 503 506 509 514 517 520 525 530 533 536 539 545 547 549 552 554 556 558 560 565 568 570 574 576 584 586 589 591 595 598 607 616 619 622 627 632 637 644 649 654 657 660 663 666 669 672 675 678 681 684 687 690 693 696 699 704 707 710 715 720 723 726 729 735 737 739 742 744 746 748 750 755 758 760 764 766 774 776 779 781 785 788 797 806 809 812 817 822 827 834 839 844 847 850 853 856 859 862 865 868 871 874 877 880 883 886 889 894 897 900 905 910 913 916 919 925 927 929 932 934 936 938 940 945 948 950 954 956 964 966 969 971 975 978 987 996 999 1002 1007 1012 1017 1024 1029 1034 1037 1040 1043 1046 1049 1052 1055 1058 1061 1064 1067 1070 1073 1078 1083 1086 1089 1094 1099 1102 1105 1108 1114 1116 1118 1123 1125 1127 1129 1131 1136 1139 1141 1145 1147 1155 1157 1160 1162 1165 1167 1170 1173 1182 1191 1194 1197 1202 1207 1212 1219 1224 1229 1232 1235 1238 1241 1244 1247 1250 1253 1256 1259 1262 1265 1268 1273 1278 1281 1284 1287 1290 1293 1296 1299 1306 1308 1310 1313 1315 1317 1319 1321 1327 1329 1331 1335 1337 1341 1344 1346 1348 1350 1353 
#Tower 1 Storey 1: Beam_Element i_Joint_Element Region
region 3 -ele 1 3 6 8 11 13 16 18 21 23 26 29 32 34 37 39 42 44 47 49 51 54 56 59 61 64 67 70 73 76 79 82 85 88 91 94 97 100 103 106 109 111 114 117 120 122 125 127 128 131 134 137 140 142 143 144 146 148 150 151 153 155 156 158 160 162 164 166 167 168 169 170 171 174 176 177 178 179 180 181 184 186 189 191 1360 1364 
#Tower 1 Storey 1: Beam_Element j_Joint_Element Region
region 4 -ele 2 5 7 10 12 15 17 20 22 25 28 31 33 36 38 41 43 46 48 50 53 55 58 60 63 66 69 72 75 78 81 84 87 90 93 96 99 102 105 108 110 113 116 119 121 124 126 127 130 133 136 139 141 142 143 145 147 149 150 152 154 155 157 159 161 163 165 166 167 168 169 170 173 175 176 177 178 179 180 183 185 188 190 191 1360 1364 
#Tower 1 Storey 2: Beam_Element i_Joint_Element Region
region 5 -ele 192 194 197 199 201 203 206 208 210 212 215 218 221 223 226 228 231 233 236 238 240 243 245 248 250 253 256 259 262 265 268 271 274 277 280 283 286 289 292 295 298 300 303 306 309 311 314 316 317 320 323 325 328 330 331 333 335 337 340 341 342 344 346 348 351 353 355 358 360 362 365 366 368 370 372 374 376 377 378 379 380 382 385 387 388 389 390 391 392 395 397 400 402 1355 1365 
#Tower 1 Storey 2: Beam_Element j_Joint_Element Region
region 6 -ele 193 196 198 200 202 205 207 209 211 214 217 220 222 225 227 230 232 235 237 239 242 244 247 249 252 255 258 261 264 267 270 273 276 279 282 285 288 291 294 297 299 302 305 308 310 313 315 316 319 322 324 327 329 330 331 334 336 338 340 341 342 344 347 349 351 353 356 358 361 363 365 367 369 371 373 375 376 377 378 379 380 384 386 387 388 389 390 391 394 396 399 401 402 1355 1365 
#Tower 1 Storey 3: Beam_Element i_Joint_Element Region
region 7 -ele 403 406 409 411 413 415 418 420 422 424 427 430 433 435 438 440 443 445 448 450 452 455 457 460 462 465 468 471 474 477 480 483 486 489 492 495 498 501 504 507 510 512 515 518 521 523 526 528 531 534 537 540 542 543 544 546 548 550 551 553 555 557 559 561 562 563 564 566 567 569 571 572 575 577 578 579 580 581 582 585 587 590 592 1356 1357 1366 1367 
#Tower 1 Storey 3: Beam_Element j_Joint_Element Region
region 8 -ele 405 408 410 412 414 417 419 421 423 426 429 432 434 437 439 442 444 447 449 451 454 456 459 461 464 467 470 473 476 479 482 485 488 491 494 497 500 503 506 509 511 514 517 520 522 525 527 530 533 536 539 541 542 543 545 547 549 550 552 554 556 558 560 561 562 563 565 566 568 570 571 574 576 577 578 579 580 581 584 586 589 591 592 1356 1357 1366 1367 
#Tower 1 Storey 4: Beam_Element i_Joint_Element Region
region 9 -ele 593 596 599 601 603 605 608 610 612 614 617 620 623 625 628 630 633 635 638 640 642 645 647 650 652 655 658 661 664 667 670 673 676 679 682 685 688 691 694 697 700 702 705 708 711 713 716 718 721 724 727 730 732 733 734 736 738 740 741 743 745 747 749 751 752 753 754 756 757 759 761 762 765 767 768 769 770 771 772 775 777 780 782 1358 1359 1368 1369 
#Tower 1 Storey 4: Beam_Element j_Joint_Element Region
region 10 -ele 595 598 600 602 604 607 609 611 613 616 619 622 624 627 629 632 634 637 639 641 644 646 649 651 654 657 660 663 666 669 672 675 678 681 684 687 690 693 696 699 701 704 707 710 712 715 717 720 723 726 729 731 732 733 735 737 739 740 742 744 746 748 750 751 752 753 755 756 758 760 761 764 766 767 768 769 770 771 774 776 779 781 782 1358 1359 1368 1369 
#Tower 1 Storey 5: Beam_Element i_Joint_Element Region
region 11 -ele 783 786 789 791 793 795 798 800 802 804 807 810 813 815 818 820 823 825 828 830 832 835 837 840 842 845 848 851 854 857 860 863 866 869 872 875 878 881 884 887 890 892 895 898 901 903 906 908 911 914 917 920 922 923 924 926 928 930 931 933 935 937 939 941 942 943 944 946 947 949 951 952 955 957 958 959 960 961 962 965 967 970 972 1363 1373 
#Tower 1 Storey 5: Beam_Element j_Joint_Element Region
region 12 -ele 785 788 790 792 794 797 799 801 803 806 809 812 814 817 819 822 824 827 829 831 834 836 839 841 844 847 850 853 856 859 862 865 868 871 874 877 880 883 886 889 891 894 897 900 902 905 907 910 913 916 919 921 922 923 925 927 929 930 932 934 936 938 940 941 942 943 945 946 948 950 951 954 956 957 958 959 960 961 964 966 969 971 972 1363 1373 
#Tower 1 Storey 6: Beam_Element i_Joint_Element Region
region 13 -ele 973 976 979 981 983 985 988 990 992 994 997 1000 1003 1005 1008 1010 1013 1015 1018 1020 1022 1025 1027 1030 1032 1035 1038 1041 1044 1047 1050 1053 1056 1059 1062 1065 1068 1071 1074 1076 1079 1081 1084 1087 1090 1092 1095 1097 1100 1103 1106 1109 1111 1112 1113 1115 1117 1120 1122 1124 1126 1128 1130 1132 1133 1134 1135 1137 1138 1140 1142 1143 1146 1148 1149 1150 1151 1152 1153 1156 1158 1161 1163 1164 1166 1362 1372 
#Tower 1 Storey 6: Beam_Element j_Joint_Element Region
region 14 -ele 975 978 980 982 984 987 989 991 993 996 999 1002 1004 1007 1009 1012 1014 1017 1019 1021 1024 1026 1029 1031 1034 1037 1040 1043 1046 1049 1052 1055 1058 1061 1064 1067 1070 1073 1075 1078 1080 1083 1086 1089 1091 1094 1096 1099 1102 1105 1108 1110 1111 1112 1114 1116 1118 1120 1123 1125 1127 1129 1131 1132 1133 1134 1136 1137 1139 1141 1142 1145 1147 1148 1149 1150 1151 1152 1155 1157 1160 1162 1163 1165 1167 1362 1372 
#Tower 1 Storey 7: Beam_Element i_Joint_Element Region
region 15 -ele 1168 1171 1174 1176 1178 1180 1183 1185 1187 1189 1192 1195 1198 1200 1203 1205 1208 1210 1213 1215 1217 1220 1222 1225 1227 1230 1233 1236 1239 1242 1245 1248 1251 1254 1257 1260 1263 1266 1269 1271 1274 1276 1279 1282 1285 1288 1291 1294 1297 1300 1302 1303 1304 1305 1307 1309 1311 1312 1314 1316 1318 1320 1322 1323 1324 1325 1328 1330 1332 1333 1336 1338 1339 1342 1345 1347 1349 1351 1352 1354 1361 1370 1371 
#Tower 1 Storey 7: Beam_Element j_Joint_Element Region
region 16 -ele 1170 1173 1175 1177 1179 1182 1184 1186 1188 1191 1194 1197 1199 1202 1204 1207 1209 1212 1214 1216 1219 1221 1224 1226 1229 1232 1235 1238 1241 1244 1247 1250 1253 1256 1259 1262 1265 1268 1270 1273 1275 1278 1281 1284 1287 1290 1293 1296 1299 1301 1302 1303 1304 1306 1308 1310 1311 1313 1315 1317 1319 1321 1322 1323 1324 1327 1329 1331 1332 1335 1337 1338 1341 1344 1346 1348 1350 1351 1353 1354 1361 1370 1371 
#Tower 1 Storey All: Column_Disp_Element i_Joint_Element Region
region 17 -ele 1374 1377 1380 1383 1386 1389 1392 1395 1398 1401 1404 1407 1410 1413 1416 1419 1422 1425 1428 1431 1434 1437 1440 1443 1446 1449 1452 1455 1458 1461 1464 1467 1470 1473 1476 1479 1482 1485 1488 1491 1494 1497 1500 1503 1506 1509 1512 1515 1518 1521 1524 1527 1530 1533 1536 1539 1542 1545 1548 1551 1554 1557 1560 1563 1566 1569 1572 1575 1578 1581 1584 1585 1586 1587 1588 1589 1590 1591 1592 1593 1594 1595 1596 1597 1598 1599 1600 1601 1602 1603 1604 1605 1606 1607 1608 1609 1610 1611 1612 1613 1614 1615 1616 1617 1618 1619 1620 1621 1622 1623 1624 1625 1626 1627 1628 1629 1630 1631 1632 1633 1634 1635 1636 1637 1638 1639 1640 1641 1642 1643 1644 1645 1646 1647 1648 1649 1650 1651 1652 1653 1654 1655 1656 1657 1658 1659 1660 1661 1662 1663 1664 1665 1666 1667 1668 1669 1670 1671 1672 1673 1674 1675 1676 1677 1678 1679 1680 1681 1682 1683 1684 1685 1686 1687 1688 1689 1690 1691 1692 1693 1694 1695 1696 1697 1698 1699 1700 1701 1702 1703 1704 1705 1706 1707 1708 1709 1710 1711 1712 1713 1714 1715 1716 1717 1718 1719 1720 1721 1722 1723 1724 1725 1726 1727 1728 1729 1730 1731 1732 1733 1734 1735 1736 1737 1738 1739 1740 1741 1742 1743 1744 1745 1746 1747 1748 1749 1750 1751 1752 1753 1754 1755 1756 1757 1758 
#Tower 1 Storey All: Column_Disp_Element j_Joint_Element Region
region 18 -ele 1376 1379 1382 1385 1388 1391 1394 1397 1400 1403 1406 1409 1412 1415 1418 1421 1424 1427 1430 1433 1436 1439 1442 1445 1448 1451 1454 1457 1460 1463 1466 1469 1472 1475 1478 1481 1484 1487 1490 1493 1496 1499 1502 1505 1508 1511 1514 1517 1520 1523 1526 1529 1532 1535 1538 1541 1544 1547 1550 1553 1556 1559 1562 1565 1568 1571 1574 1577 1580 1583 1584 1585 1586 1587 1588 1589 1590 1591 1592 1593 1594 1595 1596 1597 1598 1599 1600 1601 1602 1603 1604 1605 1606 1607 1608 1609 1610 1611 1612 1613 1614 1615 1616 1617 1618 1619 1620 1621 1622 1623 1624 1625 1626 1627 1628 1629 1630 1631 1632 1633 1634 1635 1636 1637 1638 1639 1640 1641 1642 1643 1644 1645 1646 1647 1648 1649 1650 1651 1652 1653 1654 1655 1656 1657 1658 1659 1660 1661 1662 1663 1664 1665 1666 1667 1668 1669 1670 1671 1672 1673 1674 1675 1676 1677 1678 1679 1680 1681 1682 1683 1684 1685 1686 1687 1688 1689 1690 1691 1692 1693 1694 1695 1696 1697 1698 1699 1700 1701 1702 1703 1704 1705 1706 1707 1708 1709 1710 1711 1712 1713 1714 1715 1716 1717 1718 1719 1720 1721 1722 1723 1724 1725 1726 1727 1728 1729 1730 1731 1732 1733 1734 1735 1736 1737 1738 1739 1740 1741 1742 1743 1744 1745 1746 1747 1748 1749 1750 1751 1752 1753 1754 1755 1756 1757 1758 
#Tower 1 Storey 1: Column_Element i_Joint_Element Region
region 19 -ele 1374 1377 1380 1383 1386 1389 1392 1395 1398 1401 1404 1407 1410 1413 1416 1419 1422 1425 1428 1431 1434 1437 1440 1443 1446 1449 1452 1455 1458 1461 1464 1467 1470 1473 1476 
#Tower 1 Storey 1: Column_Element j_Joint_Element Region
region 20 -ele 1376 1379 1382 1385 1388 1391 1394 1397 1400 1403 1406 1409 1412 1415 1418 1421 1424 1427 1430 1433 1436 1439 1442 1445 1448 1451 1454 1457 1460 1463 1466 1469 1472 1475 1478 
#Tower 1 Storey 2: Column_Element i_Joint_Element Region
region 21 -ele 1479 1482 1485 1488 1491 1494 1497 1500 1503 1506 1509 1512 1515 1518 1521 1524 1527 1530 1533 1536 1539 1542 1545 1548 1551 1554 1557 1560 1563 1566 1569 1572 1575 1578 1581 
#Tower 1 Storey 2: Column_Element j_Joint_Element Region
region 22 -ele 1481 1484 1487 1490 1493 1496 1499 1502 1505 1508 1511 1514 1517 1520 1523 1526 1529 1532 1535 1538 1541 1544 1547 1550 1553 1556 1559 1562 1565 1568 1571 1574 1577 1580 1583 
#Tower 1 Storey 3: Column_Element i_Joint_Element Region
region 23 -ele 1584 1585 1586 1587 1588 1589 1590 1591 1592 1593 1594 1595 1596 1597 1598 1599 1600 1601 1602 1603 1604 1605 1606 1607 1608 1609 1610 1611 1612 1613 1614 1615 1616 1617 1618 
#Tower 1 Storey 3: Column_Element j_Joint_Element Region
region 24 -ele 1584 1585 1586 1587 1588 1589 1590 1591 1592 1593 1594 1595 1596 1597 1598 1599 1600 1601 1602 1603 1604 1605 1606 1607 1608 1609 1610 1611 1612 1613 1614 1615 1616 1617 1618 
#Tower 1 Storey 4: Column_Element i_Joint_Element Region
region 25 -ele 1619 1620 1621 1622 1623 1624 1625 1626 1627 1628 1629 1630 1631 1632 1633 1634 1635 1636 1637 1638 1639 1640 1641 1642 1643 1644 1645 1646 1647 1648 1649 1650 1651 1652 1653 
#Tower 1 Storey 4: Column_Element j_Joint_Element Region
region 26 -ele 1619 1620 1621 1622 1623 1624 1625 1626 1627 1628 1629 1630 1631 1632 1633 1634 1635 1636 1637 1638 1639 1640 1641 1642 1643 1644 1645 1646 1647 1648 1649 1650 1651 1652 1653 
#Tower 1 Storey 5: Column_Element i_Joint_Element Region
region 27 -ele 1654 1655 1656 1657 1658 1659 1660 1661 1662 1663 1664 1665 1666 1667 1668 1669 1670 1671 1672 1673 1674 1675 1676 1677 1678 1679 1680 1681 1682 1683 1684 1685 1686 1687 1688 
#Tower 1 Storey 5: Column_Element j_Joint_Element Region
region 28 -ele 1654 1655 1656 1657 1658 1659 1660 1661 1662 1663 1664 1665 1666 1667 1668 1669 1670 1671 1672 1673 1674 1675 1676 1677 1678 1679 1680 1681 1682 1683 1684 1685 1686 1687 1688 
#Tower 1 Storey 6: Column_Element i_Joint_Element Region
region 29 -ele 1689 1690 1691 1692 1693 1694 1695 1696 1697 1698 1699 1700 1701 1702 1703 1704 1705 1706 1707 1708 1709 1710 1711 1712 1713 1714 1715 1716 1717 1718 1719 1720 1721 1722 1723 
#Tower 1 Storey 6: Column_Element j_Joint_Element Region
region 30 -ele 1689 1690 1691 1692 1693 1694 1695 1696 1697 1698 1699 1700 1701 1702 1703 1704 1705 1706 1707 1708 1709 1710 1711 1712 1713 1714 1715 1716 1717 1718 1719 1720 1721 1722 1723 
#Tower 1 Storey 7: Column_Element i_Joint_Element Region
region 31 -ele 1724 1725 1726 1727 1728 1729 1730 1731 1732 1733 1734 1735 1736 1737 1738 1739 1740 1741 1742 1743 1744 1745 1746 1747 1748 1749 1750 1751 1752 1753 1754 1755 1756 1757 1758 
#Tower 1 Storey 7: Column_Element j_Joint_Element Region
region 32 -ele 1724 1725 1726 1727 1728 1729 1730 1731 1732 1733 1734 1735 1736 1737 1738 1739 1740 1741 1742 1743 1744 1745 1746 1747 1748 1749 1750 1751 1752 1753 1754 1755 1756 1757 1758 
