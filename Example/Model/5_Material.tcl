#Concrete Mat: C15
uniaxialMaterial Concrete01 1 -10.0 -0.0009 -2.00 -0.052
#Concrete Mat: C20
uniaxialMaterial Concrete01 2 -13.4 -0.0011 -2.68 -0.012
#Concrete Mat: C25
uniaxialMaterial Concrete01 3 -16.7 -0.0012 -3.34 -0.009
#Concrete Mat: C30
uniaxialMaterial Concrete01 4 -20.1 -0.0013 -4.02 -0.007
#Concrete Mat: C35
uniaxialMaterial Concrete01 5 -23.4 -0.0015 -4.67 -0.006
#Concrete Mat: C40
uniaxialMaterial Concrete01 6 -26.8 -0.0016 -5.35 -0.006
#Concrete Mat: C45
uniaxialMaterial Concrete01 7 -29.6 -0.0018 -5.91 -0.006
#Concrete Mat: C50
uniaxialMaterial Concrete01 8 -32.3 -0.0019 -6.47 -0.006
#Concrete Mat: C55
uniaxialMaterial Concrete01 9 -35.4 -0.0020 -7.09 -0.005
#Concrete Mat: C60
uniaxialMaterial Concrete01 10 -38.4 -0.0021 -7.68 -0.005
#Concrete Mat: C65
uniaxialMaterial Concrete01 11 -41.4 -0.0023 -8.28 -0.005
#Concrete Mat: C70
uniaxialMaterial Concrete01 12 -44.4 -0.0024 -8.88 -0.005
#Concrete Mat: C75
uniaxialMaterial Concrete01 13 -47.3 -0.0025 -9.46 -0.005
#Concrete Mat: C80
uniaxialMaterial Concrete01 14 -50.1 -0.0026 -10.01 -0.005
#Concrete Mat: CC30-1-30
uniaxialMaterial Concrete01 15 -20.0 -0.0013 -4.01 -0.028
#Concrete Mat: CC30-1-40
uniaxialMaterial Concrete01 16 -20.0 -0.0013 -4.01 -0.022
#Concrete Mat: CC30-1-70
uniaxialMaterial Concrete01 17 -20.0 -0.0013 -4.01 -0.013
#Concrete Mat: CC30-1-100
uniaxialMaterial Concrete01 18 -20.0 -0.0013 -4.01 -0.009
#Concrete Mat: CC30-1.1-30
uniaxialMaterial Concrete01 19 -22.1 -0.0015 -4.41 -0.029
#Concrete Mat: CC30-1.1-40
uniaxialMaterial Concrete01 20 -22.1 -0.0015 -4.41 -0.022
#Concrete Mat: CC35-1-30
uniaxialMaterial Concrete01 21 -23.3 -0.0015 -4.67 -0.028
#Concrete Mat: CC35-1-40
uniaxialMaterial Concrete01 22 -23.3 -0.0015 -4.67 -0.022
#Concrete Mat: CC35-1.1-20
uniaxialMaterial Concrete01 23 -25.7 -0.0016 -5.14 -0.042
#Concrete Mat: CC35-1.1-30
uniaxialMaterial Concrete01 24 -25.7 -0.0016 -5.14 -0.029
#Steel Mat: HPB235
uniaxialMaterial Steel02 25 235 210000 0.01 20 0.925 0.15
#Steel Mat: HPB300
uniaxialMaterial Steel02 26 300 210000 0.01 20 0.925 0.15
#Steel Mat: HRB335
uniaxialMaterial Steel02 27 335 200000 0.01 20 0.925 0.15
#Steel Mat: HRB400
uniaxialMaterial Steel02 28 400 200000 0.01 20 0.925 0.15
#Steel Mat: HRB500
uniaxialMaterial Steel02 29 500 200000 0.01 20 0.925 0.15
#Steel Mat: HRB550
uniaxialMaterial Steel02 30 550 200000 0.01 20 0.925 0.15
#Steel Mat: HTRB600
uniaxialMaterial Steel02 31 600 200000 0.01 20 0.925 0.15
#Steel Mat: HTRB630
uniaxialMaterial Steel02 32 630 200000 0.01 20 0.925 0.15
#Steel Mat: Q235
uniaxialMaterial Steel02 33 235 206000 0.03 20 0.925 0.15
#Steel Mat: Q345
uniaxialMaterial Steel02 34 345 206000 0.03 20 0.925 0.15
#Steel Mat: Q390
uniaxialMaterial Steel02 35 390 206000 0.03 20 0.925 0.15
#Steel Mat: Q420
uniaxialMaterial Steel02 36 420 206000 0.03 20 0.925 0.15
#Steel Mat: Q460
uniaxialMaterial Steel02 37 460 206000 0.03 20 0.925 0.15
#Steel Mat: Q500
uniaxialMaterial Steel02 38 500 206000 0.03 20 0.925 0.15
#Steel Mat: Q550
uniaxialMaterial Steel02 39 550 206000 0.03 20 0.925 0.15
#Steel Mat: Q620
uniaxialMaterial Steel02 40 620 206000 0.03 20 0.925 0.15
#Steel Mat: Q690
uniaxialMaterial Steel02 41 690 206000 0.03 20 0.925 0.15
#Elastic Mat: C15E
uniaxialMaterial Elastic 42 22000
#Elastic Mat: C20E
uniaxialMaterial Elastic 43 25500
#Elastic Mat: C25E
uniaxialMaterial Elastic 44 28000
#Elastic Mat: C30E
uniaxialMaterial Elastic 45 30000
#Elastic Mat: C35E
uniaxialMaterial Elastic 46 31500
#Elastic Mat: C40E
uniaxialMaterial Elastic 47 32500
#Elastic Mat: C45E
uniaxialMaterial Elastic 48 33500
#Elastic Mat: C50E
uniaxialMaterial Elastic 49 34500
#Elastic Mat: C55E
uniaxialMaterial Elastic 50 35500
#Elastic Mat: C60E
uniaxialMaterial Elastic 51 36000
#Elastic Mat: C65E
uniaxialMaterial Elastic 52 36500
#Elastic Mat: C70E
uniaxialMaterial Elastic 53 37000
#Elastic Mat: C75E
uniaxialMaterial Elastic 54 37500
#Elastic Mat: C80E
uniaxialMaterial Elastic 55 38000
#Elastic Mat: Steel
uniaxialMaterial Elastic 56 200000
#Elastic Mat: Release
uniaxialMaterial Elastic 57 1
#Elastic Mat: Rigid
uniaxialMaterial Elastic 58 1E+15
