proc Recorder_Proc { dir_name } {
file mkdir $dir_name
set dir1 $dir_name; append dir1 "/Global"
file mkdir $dir1
set dir2 $dir_name; append dir2 "/Beam"
file mkdir $dir2
set dir3 $dir_name; append dir3 "/Column"
file mkdir $dir3
set fn1 $dir_name; append fn1 "/Global" ;append fn1 "/Disp.xml"
recorder Node -xml $fn1 -time -dT 0.1 -node 432 433 434 435 436 437 438 -dof 1 2 disp
set fn2 $dir_name; append fn2 "/Global" ;append fn2 "/Vel.xml"
recorder Node -xml $fn2 -time -dT 0.1 -node 432 433 434 435 436 437 438 -dof 1 2 vel
set fn3 $dir_name; append fn3 "/Global" ;append fn3 "/Accel.xml"
recorder Node -xml $fn3 -time -dT 0.1 -node 432 433 434 435 436 437 438 -dof 1 2 accel
set fn4 $dir_name; append fn4 "/Global" ;append fn4 "/DriftX.xml"
recorder Drift -xml $fn4 -time -dT 0.1 -iNode 432 433 434 435 436 437 438 -jNode 18 67 433 434 435 436 437 -dof 1 -perpDirn 3
set fn5 $dir_name; append fn5 "/Global" ;append fn5 "/DriftY.xml"
recorder Drift -xml $fn5 -time -dT 0.1 -iNode 432 433 434 435 436 437 438 -jNode 18 67 433 434 435 436 437 -dof 2 -perpDirn 3
set fn6 $dir_name; append fn6 "/Global" ;append fn6 "/BaseForce.xml"
recorder Node -xml $fn6 -time -dT 0.1 -node 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 -dof 1 2 3 reaction
set fn7 $dir_name; append fn7 "/Beam"; append fn7 "/T1_Disp_Element_i_strain1.xml"
recorder Element -xml $fn7 -time -dT 0.1 -region 1 section 1 fiber 0 9999 strain
set fn8 $dir_name; append fn8 "/Beam"; append fn8 "/T1_Disp_Element_i_strain2.xml"
recorder Element -xml $fn8 -time -dT 0.1 -region 1 section 1 fiber 0 -9999 strain
set fn9 $dir_name; append fn9 "/Beam"; append fn9 "/T1_Disp_Element_i_strain3.xml"
recorder Element -xml $fn9 -time -dT 0.1 -region 1 section 1 fiber 9999 0 strain
set fn10 $dir_name; append fn10 "/Beam"; append fn10 "/T1_Disp_Element_i_strain4.xml"
recorder Element -xml $fn10 -time -dT 0.1 -region 1 section 1 fiber -9999 0 strain
set fn11 $dir_name; append fn11 "/Beam"; append fn11 "/T1_Disp_Element_i_deform.xml"
recorder Element -xml $fn11 -time -dT 0.1 -region 1 section 1 deformation
set fn12 $dir_name; append fn12 "/Beam"; append fn12 "/T1_Disp_Element_j_strain1.xml"
recorder Element -xml $fn12 -time -dT 0.1 -region 2 section 5 fiber 0 9999 strain
set fn13 $dir_name; append fn13 "/Beam"; append fn13 "/T1_Disp_Element_j_strain2.xml"
recorder Element -xml $fn13 -time -dT 0.1 -region 2 section 5 fiber 0 -9999 strain
set fn14 $dir_name; append fn14 "/Beam"; append fn14 "/T1_Disp_Element_j_strain3.xml"
recorder Element -xml $fn14 -time -dT 0.1 -region 2 section 5 fiber 9999 0 strain
set fn15 $dir_name; append fn15 "/Beam"; append fn15 "/T1_Disp_Element_j_strain4.xml"
recorder Element -xml $fn15 -time -dT 0.1 -region 2 section 5 fiber -9999 0 strain
set fn16 $dir_name; append fn16 "/Beam"; append fn16 "/T1_Disp_Element_j_deform.xml"
recorder Element -xml $fn16 -time -dT 0.1 -region 2 section 5 deformation
set fn17 $dir_name; append fn17 "/Beam"; append fn17 "/T1_F1_i_localforce.xml"
recorder Element -xml $fn17 -time -dT 0.1 -region 3 localForce
set fn18 $dir_name; append fn18 "/Beam"; append fn18 "/T1_F1_j_localforce.xml"
recorder Element -xml $fn18 -time -dT 0.1 -region 4 localForce
set fn19 $dir_name; append fn19 "/Beam"; append fn19 "/T1_F2_i_localforce.xml"
recorder Element -xml $fn19 -time -dT 0.1 -region 5 localForce
set fn20 $dir_name; append fn20 "/Beam"; append fn20 "/T1_F2_j_localforce.xml"
recorder Element -xml $fn20 -time -dT 0.1 -region 6 localForce
set fn21 $dir_name; append fn21 "/Beam"; append fn21 "/T1_F3_i_localforce.xml"
recorder Element -xml $fn21 -time -dT 0.1 -region 7 localForce
set fn22 $dir_name; append fn22 "/Beam"; append fn22 "/T1_F3_j_localforce.xml"
recorder Element -xml $fn22 -time -dT 0.1 -region 8 localForce
set fn23 $dir_name; append fn23 "/Beam"; append fn23 "/T1_F4_i_localforce.xml"
recorder Element -xml $fn23 -time -dT 0.1 -region 9 localForce
set fn24 $dir_name; append fn24 "/Beam"; append fn24 "/T1_F4_j_localforce.xml"
recorder Element -xml $fn24 -time -dT 0.1 -region 10 localForce
set fn25 $dir_name; append fn25 "/Beam"; append fn25 "/T1_F5_i_localforce.xml"
recorder Element -xml $fn25 -time -dT 0.1 -region 11 localForce
set fn26 $dir_name; append fn26 "/Beam"; append fn26 "/T1_F5_j_localforce.xml"
recorder Element -xml $fn26 -time -dT 0.1 -region 12 localForce
set fn27 $dir_name; append fn27 "/Beam"; append fn27 "/T1_F6_i_localforce.xml"
recorder Element -xml $fn27 -time -dT 0.1 -region 13 localForce
set fn28 $dir_name; append fn28 "/Beam"; append fn28 "/T1_F6_j_localforce.xml"
recorder Element -xml $fn28 -time -dT 0.1 -region 14 localForce
set fn29 $dir_name; append fn29 "/Beam"; append fn29 "/T1_F7_i_localforce.xml"
recorder Element -xml $fn29 -time -dT 0.1 -region 15 localForce
set fn30 $dir_name; append fn30 "/Beam"; append fn30 "/T1_F7_j_localforce.xml"
recorder Element -xml $fn30 -time -dT 0.1 -region 16 localForce
set fn31 $dir_name; append fn31 "/Column"; append fn31 "/T1_Disp_Element_i_strain1.xml"
recorder Element -xml $fn31 -time -dT 0.1 -region 17 section 1 fiber 0 9999 strain
set fn32 $dir_name; append fn32 "/Column"; append fn32 "/T1_Disp_Element_i_strain2.xml"
recorder Element -xml $fn32 -time -dT 0.1 -region 17 section 1 fiber 0 -9999 strain
set fn33 $dir_name; append fn33 "/Column"; append fn33 "/T1_Disp_Element_i_strain3.xml"
recorder Element -xml $fn33 -time -dT 0.1 -region 17 section 1 fiber 9999 0 strain
set fn34 $dir_name; append fn34 "/Column"; append fn34 "/T1_Disp_Element_i_strain4.xml"
recorder Element -xml $fn34 -time -dT 0.1 -region 17 section 1 fiber -9999 0 strain
set fn35 $dir_name; append fn35 "/Column"; append fn35 "/T1_Disp_Element_i_deform.xml"
recorder Element -xml $fn35 -time -dT 0.1 -region 17 section 1 deformation
set fn36 $dir_name; append fn36 "/Column"; append fn36 "/T1_Disp_Element_j_strain1.xml"
recorder Element -xml $fn36 -time -dT 0.1 -region 18 section 5 fiber 0 9999 strain
set fn37 $dir_name; append fn37 "/Column"; append fn37 "/T1_Disp_Element_j_strain2.xml"
recorder Element -xml $fn37 -time -dT 0.1 -region 18 section 5 fiber 0 -9999 strain
set fn38 $dir_name; append fn38 "/Column"; append fn38 "/T1_Disp_Element_j_strain3.xml"
recorder Element -xml $fn38 -time -dT 0.1 -region 18 section 5 fiber 9999 0 strain
set fn39 $dir_name; append fn39 "/Column"; append fn39 "/T1_Disp_Element_j_strain4.xml"
recorder Element -xml $fn39 -time -dT 0.1 -region 18 section 5 fiber -9999 0 strain
set fn40 $dir_name; append fn40 "/Column"; append fn40 "/T1_Disp_Element_j_deform.xml"
recorder Element -xml $fn40 -time -dT 0.1 -region 18 section 5 deformation
set fn41 $dir_name; append fn41 "/Column"; append fn41 "/T1_F1_i_localforce.xml"
recorder Element -xml $fn41 -time -dT 0.1 -region 19 localForce
set fn42 $dir_name; append fn42 "/Column"; append fn42 "/T1_F1_j_localforce.xml"
recorder Element -xml $fn42 -time -dT 0.1 -region 20 localForce
set fn43 $dir_name; append fn43 "/Column"; append fn43 "/T1_F2_i_localforce.xml"
recorder Element -xml $fn43 -time -dT 0.1 -region 21 localForce
set fn44 $dir_name; append fn44 "/Column"; append fn44 "/T1_F2_j_localforce.xml"
recorder Element -xml $fn44 -time -dT 0.1 -region 22 localForce
set fn45 $dir_name; append fn45 "/Column"; append fn45 "/T1_F3_i_localforce.xml"
recorder Element -xml $fn45 -time -dT 0.1 -region 23 localForce
set fn46 $dir_name; append fn46 "/Column"; append fn46 "/T1_F3_j_localforce.xml"
recorder Element -xml $fn46 -time -dT 0.1 -region 24 localForce
set fn47 $dir_name; append fn47 "/Column"; append fn47 "/T1_F4_i_localforce.xml"
recorder Element -xml $fn47 -time -dT 0.1 -region 25 localForce
set fn48 $dir_name; append fn48 "/Column"; append fn48 "/T1_F4_j_localforce.xml"
recorder Element -xml $fn48 -time -dT 0.1 -region 26 localForce
set fn49 $dir_name; append fn49 "/Column"; append fn49 "/T1_F5_i_localforce.xml"
recorder Element -xml $fn49 -time -dT 0.1 -region 27 localForce
set fn50 $dir_name; append fn50 "/Column"; append fn50 "/T1_F5_j_localforce.xml"
recorder Element -xml $fn50 -time -dT 0.1 -region 28 localForce
set fn51 $dir_name; append fn51 "/Column"; append fn51 "/T1_F6_i_localforce.xml"
recorder Element -xml $fn51 -time -dT 0.1 -region 29 localForce
set fn52 $dir_name; append fn52 "/Column"; append fn52 "/T1_F6_j_localforce.xml"
recorder Element -xml $fn52 -time -dT 0.1 -region 30 localForce
set fn53 $dir_name; append fn53 "/Column"; append fn53 "/T1_F7_i_localforce.xml"
recorder Element -xml $fn53 -time -dT 0.1 -region 31 localForce
set fn54 $dir_name; append fn54 "/Column"; append fn54 "/T1_F7_j_localforce.xml"
recorder Element -xml $fn54 -time -dT 0.1 -region 32 localForce
}
